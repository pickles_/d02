//
// Created by Dmitry Titenko on 11/1/17.
//

#include <iostream>
#include <cmath>
#include "Fixed.hpp"

Fixed::Fixed() : val(0)
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(const int val)
{
	std::cout << "Int constructor called" << std::endl;
	this->val = val << bits;
}

Fixed::Fixed(float val)
{
	std::cout << "Float constructor called" << std::endl;
	this->val = roundf(val * (1 << bits));
}

Fixed::Fixed(const Fixed &rhs)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = rhs;
}

Fixed::~Fixed()
{
	std::cout << "Destructor called" << std::endl;
}

bool Fixed::operator==(const Fixed &rhs) const
{
	return val == rhs.val;
}

bool Fixed::operator!=(const Fixed &rhs) const
{
	return !(rhs == *this);
}

int Fixed::getRawBits(void) const
{
	return val;
}

void Fixed::setRawBits(int const raw)
{ val = raw; }

Fixed &Fixed::operator=(const Fixed &rhs)
{
	std::cout << "Assignation operator called" << std::endl;
	if (this == &rhs)
		return (*this);
	val = rhs.getRawBits();
	return (*this);
}


float Fixed::toFloat() const
{
	float v = val;
	return v / (1 << bits);
}

int Fixed::toInt() const
{
	return val >> bits;
}

std::ostream &operator<<(std::ostream &os, const Fixed &rhs)
{
	os << rhs.toFloat();
	return os;
}