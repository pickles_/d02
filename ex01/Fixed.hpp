//
// Created by Dmitry Titenko on 11/1/17.
//

#ifndef PROJECT_FIXED_HPP
#define PROJECT_FIXED_HPP


#include <ostream>

class Fixed
{
	private:
		int val;
		static const int bits = 8;
	public:
		Fixed();
		Fixed(int val);
		Fixed(float val);
		Fixed(const Fixed &rhs);
		virtual ~Fixed();

		int getRawBits( void ) const;
		void setRawBits( int const raw );

		bool operator==(const Fixed &rhs) const;
		bool operator!=(const Fixed &rhs) const;
		Fixed &operator=(const Fixed &rhs);

		float toFloat() const;
		int toInt() const;
};

std::ostream &operator<<(std::ostream &os, const Fixed &rhs);


#endif //PROJECT_FIXED_HPP
