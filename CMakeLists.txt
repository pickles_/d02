cmake_minimum_required(VERSION 3.8)

project(d02)

set(CMAKE_CXX_STANDARD 98)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Werror")
message(STATUS ${CMAKE_CXX_FLAGS})

add_executable(ex00 ex00/Fixed.cpp ex00/main.cpp)
add_executable(ex01 ex01/Fixed.cpp ex01/main.cpp)
add_executable(ex02 ex02/Fixed.cpp ex02/main.cpp)