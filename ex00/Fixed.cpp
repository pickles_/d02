//
// Created by Dmitry Titenko on 11/1/17.
//

#include <iostream>
#include "Fixed.hpp"

Fixed::Fixed(int val) : val(val)
{}

Fixed::Fixed() : val(0)
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(const Fixed &rhs)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = rhs;
}

Fixed::~Fixed()
{
	std::cout << "Destructor called" << std::endl;
}

bool Fixed::operator==(const Fixed &rhs) const
{
	return val == rhs.val;
}

bool Fixed::operator!=(const Fixed &rhs) const
{
	return !(rhs == *this);
}

int Fixed::getRawBits(void) const
{
	std::cout << "getRawBits member function called" << std::endl;
	return val;
}

void Fixed::setRawBits(int const raw)
{ val = raw; }

Fixed &Fixed::operator=(const Fixed &rhs)
{
	std::cout << "Assignation operator called" << std::endl;
	if (this == &rhs)
		return (*this);
	val = rhs.getRawBits();
	return (*this);
}
