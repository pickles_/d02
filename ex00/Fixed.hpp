//
// Created by Dmitry Titenko on 11/1/17.
//

#ifndef PROJECT_FIXED_HPP
#define PROJECT_FIXED_HPP


class Fixed
{
	private:
		int val;
		static const int bits = 8;
	public:
		Fixed(int val);
		Fixed();
		Fixed(const Fixed &rhs);
		virtual ~Fixed();

		int getRawBits( void ) const;
		void setRawBits( int const raw );

		bool operator==(const Fixed &rhs) const;
		bool operator!=(const Fixed &rhs) const;
		Fixed &operator=(const Fixed &rhs);
};


#endif //PROJECT_FIXED_HPP
