//
// Created by Dmitry Titenko on 11/1/17.
//

#include <iostream>
#include <cmath>
#include "Fixed.hpp"

Fixed::Fixed() : val(0)
{
}

Fixed::Fixed(const int val)
{
	this->val = val << bits;
}

Fixed::Fixed(float val)
{
	this->val = roundf(val * (1 << bits));
}

Fixed::Fixed(const Fixed &rhs)
{
	*this = rhs;
}

Fixed::~Fixed()
{}

//Getters and Setters
int Fixed::getRawBits(void) const
{
	return val;
}

void Fixed::setRawBits(int const raw)
{ val = raw; }

//Converters
float Fixed::toFloat() const
{
	float v = val;
	return v / (1 << bits);
}

int Fixed::toInt() const
{
	return val >> bits;
}

//Operators
Fixed &Fixed::operator=(const Fixed &rhs)
{
	if (this == &rhs)
		return (*this);
	val = rhs.getRawBits();
	return (*this);
}

bool Fixed::operator==(const Fixed &rhs) const
{
	return val == rhs.val;
}

bool Fixed::operator!=(const Fixed &rhs) const
{
	return !(rhs == *this);
}

bool Fixed::operator<=(const Fixed &rhs) const
{
	return val <= rhs.val;
}

bool Fixed::operator>=(const Fixed &rhs) const
{
	return val >= rhs.val;
}

bool Fixed::operator>(const Fixed &rhs) const
{
	return val > rhs.val;
}

bool Fixed::operator<(const Fixed &rhs) const
{
	return val < rhs.val;
}

Fixed Fixed::operator+(const Fixed &rhs) const
{
	Fixed n;

	n.val = val + rhs.val;
	return n;
}

Fixed Fixed::operator-(const Fixed &rhs) const
{
	Fixed n;

	n.val = val - rhs.val;
	return n;
}

Fixed Fixed::operator/(const Fixed &rhs) const
{
	Fixed n;

	n.val = (val << bits) / rhs.val;
	return n;
}

Fixed Fixed::operator*(const Fixed &rhs) const
{
	Fixed n;

	n.val = (val * rhs.val) >> bits;
	return n;
}

Fixed &Fixed::operator++()
{
	val++;
	return *this;
}

Fixed &Fixed::operator--()
{
	val--;
	return *this;
}

Fixed Fixed::operator++(int)
{
	Fixed n(*this);
	this->val++;
	return n;
}

Fixed Fixed::operator--(int)
{
	Fixed n(*this);
	this->val--;
	return n;
}

std::ostream &operator<<(std::ostream &os, const Fixed &rhs)
{
	os << rhs.toFloat();
	return os;
}


//Static
Fixed &Fixed::min(Fixed &lhs, Fixed &rhs)
{
	return lhs < rhs ? lhs : rhs;
}

Fixed &Fixed::max(Fixed &lhs, Fixed &rhs)
{
	return lhs < rhs ? rhs : lhs;
}

const Fixed &Fixed::min(const Fixed &lhs, const Fixed &rhs)
{
	return lhs < rhs ? lhs : rhs;
}

const Fixed &Fixed::max(const Fixed &lhs, const Fixed &rhs)
{
	return lhs < rhs ? rhs : lhs;
}