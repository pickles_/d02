//
// Created by Dmitry Titenko on 11/1/17.
//

#ifndef PROJECT_FIXED_HPP
#define PROJECT_FIXED_HPP


#include <ostream>

class Fixed
{
	private:
		int val;
		static const int bits = 8;

	public:
		Fixed();
		Fixed(int val);
		Fixed(float val);
		Fixed(const Fixed &rhs);
		virtual ~Fixed();

		int getRawBits(void) const;
		void setRawBits(int const raw);
		float toFloat() const;
		int toInt() const;

		bool operator==(const Fixed &rhs) const;
		bool operator!=(const Fixed &rhs) const;
		bool operator<=(const Fixed &rhs) const;
		bool operator>=(const Fixed &rhs) const;
		bool operator> (const Fixed &rhs) const;
		bool operator< (const Fixed &rhs) const;

		Fixed operator+(const Fixed &rhs) const;
		Fixed operator-(const Fixed &rhs) const;
		Fixed operator/(const Fixed &rhs) const;
		Fixed operator*(const Fixed &rhs) const;

		Fixed &operator++();
		Fixed &operator--();
		Fixed  operator++(int);
		Fixed  operator--(int);

		Fixed &operator=(const Fixed &rhs);

		static const Fixed &min(const Fixed &lhs, const Fixed &rhs);
		static const Fixed &max(const Fixed &lhs, const Fixed &rhs);

		static Fixed &min(Fixed &lhs, Fixed &rhs);
		static Fixed &max(Fixed &lhs, Fixed &rhs);
};

std::ostream &operator<<(std::ostream &os, const Fixed &rhs);

#endif //PROJECT_FIXED_HPP
